const { MongoClient } = require('mongodb');
const config = require('./mongodbconfig');

async function connectToDatabase() {
    try {
        // Se connecter à chaque shard
        for (let shard of config.shards) {
            const client = new MongoClient(shard.mongodbUri);
            await client.connect();
            console.log(`Connected to MongoDB Shard ${shard.shardId}`);
            // Faites ici ce que vous devez faire avec chaque shard, par exemple, insérez des données, lisez des données, etc.
        }
    } catch (error) {
        console.error('Error connecting to MongoDB:', error);
    }
}

connectToDatabase();
