const { MongoClient } = require('mongodb');

// URI de connexion à MongoDB
const uri = 'mongodb://127.0.0.1:27017';


// Créer un client MongoDB
const client = new MongoClient(uri);

// Fonction pour se connecter à la base de données
async function connect() {
    try {
        // Se connecter au client MongoDB
        await client.connect();
        console.log('Connected to MongoDB');

        // Appel d'une fonction pour traiter les opérations sur la base de données
        // Par exemple, vous pouvez appeler une fonction pour effectuer des opérations sur chaque shard ici

        // Une fois que vous avez terminé les opérations sur la base de données, vous pouvez fermer la connexion
        await client.close();
        console.log('Disconnected from MongoDB');
    } catch (error) {
        console.error('Error connecting to MongoDB:', error);
    }
}

// Appeler la fonction pour se connecter à la base de données
connect();
