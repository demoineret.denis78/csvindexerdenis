module.exports = {
    shards: [
        {
            shardId: 0,
            mongodbUri: 'mongodb://127.0.0.1:27017/shard0'
        },
        {
            shardId: 1,
            mongodbUri: 'mongodb://127.0.0.1:27017/shard1'
        },
        {
            shardId: 2,
            mongodbUri: 'mongodb://127.0.0.1:27017/shard2'
        }
    ],
    replication: {
        enabled: true,
        replicationFactor: 2 // Replication factor, you can adjust this according to your needs
    },
    loadBalancer: {
        host: 'localhost',
        port: 8080 // Port for load balancer
    }
};
 