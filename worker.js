const fs = require("fs-extra");
const csv = require("convert-csv-to-json");
const mongoose = require('mongoose');
const config = require('./mongodbconfig');
const DataModel = require('./src/model'); // Importer le modèle de données

// Variable pour suivre si l'analyse des fichiers CSV a déjà été effectuée
let csvFilesProcessed = false;

// Fonction pour convertir un fichier CSV en JSON
const csvToJson = async (file) => {
  try {
    console.log(`Analyzing file ${file}`);
    let json = csv.fieldDelimiter(',').getJsonFromCsv(`chunks/csv/${file}`);
    console.log(`File ${file} has been analyzed`);

    // Supprimer les clés vides dans chaque objet JSON
    json.forEach((item) => {
      for (let key in item) {
        if (item[key] === "") {
          delete item[key];
        }
      }
    });

    // Écrire le JSON résultant dans un fichier
    fs.writeFileSync(`chunks/json/${file}.json`, JSON.stringify(json));

    return;
  } catch (error) {
    console.error(error);
  }
};

// Fonction pour analyser tous les fichiers de CSV
const parseAllFiles = async () => {
  try {
    if (csvFilesProcessed) {
      console.log("CSV files have already been processed, skipping...");
      return;
    }

    const fichiersDansDossier = fs.readdirSync("C:/Users/Soden/Music/workIndexer/chunks/csv");
    const totalFilesToProcess = fichiersDansDossier.length; // Nombre total de fichiers à traiter
    for (let i = 0; i < totalFilesToProcess; i++) {
      await csvToJson(`${i}.csv`);
    }
    csvFilesProcessed = true;
  } catch (e) {
    console.error(e);
  }
};

// Fonction pour insérer les données JSON dans la base de données
const insertDataToDatabase = async () => {
  try {
    await mongoose.connect(config.shards[0].mongodbUri, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
    console.log(`Connected to MongoDB`);

    // Récupération des fichiers JSON
    const fichiersDansDossier = fs.readdirSync("C:/Users/Soden/Music/workIndexer/chunks/json");
    // Parcourir tous les fichiers JSON et insérer les données dans la base de données
    for (let file of fichiersDansDossier) {
      console.log(`Inserting data from ${file}`);
      const jsonData = require(`./chunks/json/${file}`);
      await DataModel.insertMany(jsonData);
      console.log(`Data from ${file} inserted`);
    }

    mongoose.connection.close();
    console.log(`Connection to MongoDB closed`);
    process.exit(0); // Arrêter le programme avec un code de succès
  } catch (error) {
    console.error("Error inserting data into database:", error);
    process.exit(1); // Arrêter le programme avec un code d'erreur
  }
};

// Appel de la fonction pour analyser tous les fichiers CSV
parseAllFiles()
  .then(() => {
    console.log("All CSV files have been analyzed successfully");
    // Une fois que tous les fichiers CSV ont été analysés, insérer les données dans la base de données
    insertDataToDatabase();
  })
  .catch(error => {
    console.error("Error parsing CSV files:", error);
    process.exit(1); // Arrêter le programme avec un code d'erreur en cas d'erreur de traitement des fichiers CSV
  });
