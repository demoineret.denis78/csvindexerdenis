const fs = require('fs'); // Import the fs module for file system operations
const splitStream = require("csv-split-stream");

const splitTime = Date.now();

return splitStream.split(
    fs.createReadStream("StockEtablissementHistorique.csv"),
    { lineLimit: 100000 },
    (index) => fs.createWriteStream(`chunks/csv/${index}.csv`)
).then(csvSplitResponse => {
    const elapsedTime = Date.now() - splitTime;
    const minutes = Math.floor(elapsedTime / (1000 * 60));
    const seconds = Math.floor((elapsedTime % (1000 * 60)) / 1000);
    const milliseconds = elapsedTime % 1000;
});

