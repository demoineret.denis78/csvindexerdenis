const fs = require('fs');
const splitStream = require("csv-split-stream");

const splitCSV = (csvFileName) => {
  const splitTime = Date.now();

  return new Promise((resolve, reject) => {
    splitStream.split(
      fs.createReadStream(csvFileName),
      { lineLimit: 100000 },
      (index) => fs.createWriteStream(`chunks/csv/${index}.csv`)
    ).then(csvSplitResponse => {
      const elapsedTime = Date.now() - splitTime;
      const minutes = Math.floor(elapsedTime / (1000 * 60));
      const seconds = Math.floor((elapsedTime % (1000 * 60)) / 1000);
      const milliseconds = elapsedTime % 1000;
      console.log(`Fichier ${csvFileName} divisé en ${csvSplitResponse.length} fichiers en ${minutes}:${seconds},${milliseconds}`);
      resolve(csvSplitResponse);
    }).catch(error => {
      reject(error);
    });
  });
};

module.exports = splitCSV;
